build: cmd/ internal/
	go build ./cmd/stark

snapshot-test: build
	./snapshot_tests/test.py
integration-test: build
	cd integration_tests && ./main.py -v
lint:
	go vet ./... && staticcheck ./...