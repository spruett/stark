package = "stark"
srcs = [
    "*.star"
]

deps = {
    "t": "registry.spruett.dev/scott/stark/test:latest"
}
