load(
    '//lib.star',
    'config_map',
)
load('//config.yaml', data='raw')

export(
    config_map('app-cfg', config=data)
)