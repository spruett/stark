load('std/kube/bundle.star', 'bundle')
load(
    '//lib.star',
    'simple_deployment',
    'container',
    'container_port',
    'service_for',
)

version = ctx.get('version', 'latest')

deploy = simple_deployment(
    'nginx',
    replicas=3,
    containers=[
        container('nginx:{}'.format(version), ports=[container_port(80)])
    ],
    init_containers=[
        container('busybox:latest', command=["sleep", "15"])
    ]
)
deploy2 = simple_deployment(
    'nginx2',
    replicas=3,
    containers=[
        container('nginx:{}'.format(version), ports=[container_port(83)])
    ],
    init_containers=[
        container('busybox:latest', command=["sleep", "8"])
    ]
)
svc = service_for(deploy)

export(
    bundle(
        'nginx',
        target=struct(
            namespace='apps',
        ),
        resources=[
            deploy,
            deploy2,
            svc,
        ]
    )
)
