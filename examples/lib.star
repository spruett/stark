def container(
    image,
    name=None,
    **kwargs
):
    if not name:
        name = image.split('/')[-1].split(':')[0]
    return struct(
        name=name,
        image=image,
        **kwargs
    )

def container_port(port, **kwargs):
    return struct(
        containerPort=port,
        **kwargs,
    )

def env_vars(**kwargs):
    return [struct(name=k, value=v) for k, v in kwargs.items()]

def simple_deployment(
    name,
    containers,
    replicas=1,
    init_containers=None,
):
    labels = {'app': name}
    return struct(
        apiVersion='apps/v1',
        kind='Deployment',
        metadata=struct(
            name=name,
            labels=labels,
        ),
        spec=struct(
            replicas=replicas,
            selector=struct(matchLabels=labels),
            template=struct(
                metadata=struct(
                    labels=labels,
                ),
                spec=struct(
                    containers=containers,
                    initContainers=init_containers,
                )
            )
        )
    )

def service_for(deployment, protocol='TCP'):
    selector = deployment.spec.selector.matchLabels
    first_container = deployment.spec.template.spec.containers[0]
    ports = first_container.ports

    return struct(
        apiVersion='v1',
        kind='Service',
        metadata=struct(
            name=deployment.metadata.name,
            namespace=get(deployment.metadata, 'namespace'),
        ),
        spec=struct(
            selector=selector,
            ports=[
                struct(
                    port=port.containerPort,
                    protocol=protocol
                )
                for port in ports
            ]
        )
    )

def config_map(name, **kwargs):
    return struct(
        apiVersion='v1',
        kind='ConfigMap',
        metadata=struct(name=name),
        data=struct(**kwargs),
    )