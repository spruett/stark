load(
    '//lib.star',
    'simple_deployment',
    'container',
    'container_port',
    'service_for',
)

version = ctx.get('version', 'latest')

deploy = simple_deployment(
    'nginx',
    containers=[
        container('nginx:{}'.format(version), ports=[container_port(83)])
    ]
)
svc = service_for(deploy)

export(deploy, svc)
