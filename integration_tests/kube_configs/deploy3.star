load('std/kube/bundle.star', 'bundle')
load('//lib/lib.star', 'simple_deployment', 'container', 'env_vars')

deployment = simple_deployment(
    'nginx',
    containers=[
        container('nginx:latest', env=env_vars(ANOTHER_VAR="NEW_VAL"))
    ]
)
deployment2 = simple_deployment(
    'nginx2',
    containers=[
        container('nginx:latest'),
    ]
)

export(
    bundle(
        'nginx',
        resources=[deployment, deployment2],
    )
)