load('std/kube/bundle.star', 'bundle')
load('//lib/lib.star', 'simple_deployment', 'container', 'env_vars')

deployment = simple_deployment(
    'nginx',
    containers=[
        container('nginx:latest', env=env_vars(NEW_VAR="NEW_VAL"))
    ]
)

export(
    bundle(
        'nginx',
        resources=[deployment],
    )
)