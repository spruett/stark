load('std/kube/bundle.star', 'bundle')
load('//lib/lib.star', 'simple_deployment', 'container')

deployment = simple_deployment(
    'nginx',
    containers=[
        container('nginx:latest')
    ]
)

export(
    bundle(
        'nginx',
        resources=[deployment],
    )
)