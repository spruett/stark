import os
import subprocess

def setup_cluster(host):
    existing_clusters = subprocess.check_output(
        ['kind', 'get', 'clusters']
    ).decode('utf-8')
    if 'stark-test' in existing_clusters:
        teardown_cluster()

    subprocess.check_call([
        'kind',
        'create',
        'cluster',
        '--name=stark-test',
        '--config=./kind-config.yaml',
        '--kubeconfig=./test.kubecfg',
    ])
    fix_kubectl('./test.kubecfg', host)

def teardown_cluster():
    subprocess.check_call(['kind', 'delete', 'cluster', '--name=stark-test'])
    os.remove('./test.kubecfg')


def kubectl(*cmd):
    return subprocess.check_output(
        ['kubectl', '--kubeconfig', './test.kubecfg'] + list(cmd),
    )

def fix_kubectl(path, host):
    with open(path) as fd:
        content = fd.read()
    with open(path, 'w') as fd:
        for h in ['127.0.0.1', 'localhost', '0.0.0.0']:
            content = content.replace(h, host)
        fd.write(content)
