#!/usr/bin/env python3
import contextlib
import os
import subprocess
import unittest
import json
from urllib.parse import urlparse

import kind
import registry

stark_binary = os.path.realpath('../stark')

docker_host = 'localhost'
if os.environ.get('DOCKER_HOST'):
    url = urlparse(os.environ.get('DOCKER_HOST'))
    docker_host = url.hostname

docker_registry = docker_host + ':5000'
    

@contextlib.contextmanager
def cd(path):
   old_path = os.getcwd()
   os.chdir(path)
   try:
       yield
   finally:
       os.chdir(old_path)

def stark(*cmd):
    env = {
        'KUBECONFIG': './test.kubecfg',
        'STARK_FORCE_HTTP_REGISTRY': 'true',
        'STARK_INTEGRATION_TEST_DOCKER_HOST': docker_registry,
        'PATH': os.environ.get('PATH'),
        'HOME': os.environ.get('HOME'),
    }
    return subprocess.check_output([stark_binary] + list(cmd), env=env)

class KubeTests(unittest.TestCase):
    def setUpClass():
        kind.setup_cluster(docker_host)

    def test_deploy(self):
        stark('kube', 'deploy', '-y', 'kube_configs/deploy1.star')
        results = json.loads(
            kind.kubectl('get', 'deploy', '-ojson')
        )
        deploys = results['items']
        assert(len(deploys) == 1)
        deploy = deploys[0]
        assert(deploy['metadata']['name'] == 'nginx')

    def test_diff(self):
        stark('kube', 'deploy', '-y', 'kube_configs/deploy1.star')
        try:
            stark('kube', 'diff', 'kube_configs/deploy2.star')
            assert False, 'diff should return non-zero'
        except subprocess.CalledProcessError as exc:
            output = exc.output.decode('utf-8')
            assert 'NEW_VAR' in output and 'NEW_VAL' in output

    def test_deploy_watch(self):
        stark('kube', 'deploy', '-y', '--watch', 'kube_configs/deploy3.star')
        results = json.loads(
            kind.kubectl('get', 'deploy', '-ojson')
        )
        deploys = results['items']
        assert(len(deploys) == 2)

    def tearDownClass():
        kind.teardown_cluster()


class RegistryTests(unittest.TestCase):
    def setUpClass():
        registry.setup_registry()

    def do_push(self, pkg, tag='latest'):
        with cd(pkg):
            stark('pkg', 'clear')
            stark('pkg', 'fd')
            stark('pkg', 'push', '{}/{}:{}'.format(docker_registry, pkg, tag))

    def test_push(self):
        self.do_push('pkg1', 'test1')

    def test_single_pull(self):
        self.do_push('pkg1')
        with cd('pkg2'):
            stark('pkg', 'clear')
            stark('pkg', 'fd')
        output = stark('build', '-f', 'json', 'pkg2/test.star').decode('utf-8')
        results = json.loads(output)
        assert results['val'] == 'test'

    def test_nested_deps(self):
        self.do_push('pkg1')
        self.do_push('pkg3')
        with cd('pkg4'):
            stark('pkg', 'clear')
            stark('pkg', 'fd')
        output = stark('build', '-f', 'json', 'pkg4/test.star').decode('utf-8')
        results = json.loads(output)
        assert results['val'] == 'test-2'

    def tearDownClass():
        registry.teardown_registry()

if __name__ == '__main__':
    unittest.main()