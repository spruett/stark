package = "pkg2"

srcs = ["*.star"]

docker_host = env.get('STARK_INTEGRATION_TEST_DOCKER_HOST', 'localhost:5000')

deps = {
    'pkg3': '{}/pkg3:latest'.format(docker_host),
}
