import subprocess

def setup_registry():
    subprocess.check_call(
        ['docker', 'run', '--rm', '-d', '-p', '5000:5000', '--name', 'stark-registry-test', 'registry:2']
    )

def teardown_registry():
    subprocess.check_call(
        ['docker', 'stop', 'stark-registry-test'],
    )