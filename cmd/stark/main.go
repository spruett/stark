package main

import (
	"os"
	"strings"

	"github.com/alecthomas/kong"

	"gitlab.com/spruett/stark/internal/commands"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func getBinary() string {
	return os.Args[0]
}

func isExternalDiff() bool {
	externalDiff := os.Getenv("KUBECTL_EXTERNAL_DIFF")
	if strings.Contains(externalDiff, getBinary()) || strings.Contains(externalDiff, "stark") {
		for _, arg := range os.Args[1:] {
			if arg == "external-diff" {
				return true
			}
		}
	}
	return false
}

func fixExternalDiff() {
	if !isExternalDiff() {
		return
	}

	// If called from an external diff, fix arg ordering
	// KUBECTL_EXTERNAL_DIFF inserts arguments in the middle,
	// instead of at the end

	args := []string{os.Args[0]}
	for i := 3; i < len(os.Args); i++ {
		args = append(args, os.Args[i])
	}
	args = append(args, os.Args[1], os.Args[2])

	os.Args = args
}

func main() {
	fixExternalDiff()

	var cli commands.Cli
	args := kong.Parse(&cli)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	if cli.Trace {
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	} else if cli.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	err := args.Run()
	args.FatalIfErrorf(err)
}
