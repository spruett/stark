FROM docker:23.0.4-dind
COPY --from=golang:1.20-alpine /usr/local/go/ /usr/local/go/

RUN apk update
RUN apk add make
RUN apk add python3
RUN apk add git
RUN apk add wget
RUN apk add curl

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN mv kubectl /usr/local/bin/
RUN chmod +x /usr/local/bin/kubectl

ENV PATH="$PATH:/usr/local/go/bin:/root/go/bin"

RUN go install sigs.k8s.io/kind@v0.18.0