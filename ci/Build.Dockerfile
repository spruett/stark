FROM golang:1.20-alpine

RUN apk update
RUN apk add make
RUN apk add python3
RUN apk add git

RUN go install honnef.co/go/tools/cmd/staticcheck@latest
