package deps

import (
	"context"
	"fmt"
	"io/fs"
	"mime"
	"os"
	"path/filepath"

	dockerconfig "github.com/docker/cli/cli/config"
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"github.com/rs/zerolog/log"
	"oras.land/oras-go/v2"
	"oras.land/oras-go/v2/content/file"
	"oras.land/oras-go/v2/registry/remote"
	"oras.land/oras-go/v2/registry/remote/auth"
	"oras.land/oras-go/v2/registry/remote/retry"

	"gitlab.com/spruett/stark/internal/config"
)

func init() {
	mime.AddExtensionType(".star", "text/x-starlark")
}

func resolveSrcs(cfg *config.StarkCfg) ([]string, error) {
	srcs := make([]string, 0)
	for _, srcPattern := range cfg.Srcs {
		files, err := filepath.Glob(srcPattern)
		if err != nil {
			return nil, fmt.Errorf("failed to resolve srcs pattern: %v", err)
		}
		srcs = append(srcs, files...)
	}
	return srcs, nil
}

func resolveFilesInPkgs(cfg *config.StarkCfg) ([]string, error) {
	base := filepath.Dir(cfg.Filename)
	pkgDir := filepath.Join(base, config.DependencyDir)
	stat, _ := os.Stat(pkgDir)
	if stat == nil {
		return []string{}, nil
	}

	files := make([]string, 0)
	err := filepath.WalkDir(pkgDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			rel, err := filepath.Rel(base, path)
			if err != nil {
				return err
			}
			files = append(files, rel)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}

type RepoClient struct {
	Cfg *config.StarkCfg
}

func NewRepoClient(cfg *config.StarkCfg) *RepoClient {
	return &RepoClient{
		Cfg: cfg,
	}
}

func (rc *RepoClient) getRepository(ref string) (*remote.Repository, error) {
	repo, err := remote.NewRepository(ref)
	if err != nil {
		return nil, fmt.Errorf("failed to get remote: %v", err)
	}
	repo.Client = &auth.Client{
		Client: retry.DefaultClient,
		Cache:  auth.DefaultCache,
		Credential: func(ctx context.Context, host string) (auth.Credential, error) {
			dockerConfig := dockerconfig.LoadDefaultConfigFile(os.Stderr)
			cfg, err := dockerConfig.GetAuthConfig(repo.Reference.Registry)
			if err != nil {
				return auth.EmptyCredential, err
			}
			return auth.Credential{
				Username: cfg.Username,
				Password: cfg.Password,
			}, nil
		},
	}

	if os.Getenv("STARK_FORCE_HTTP_REGISTRY") != "" {
		repo.PlainHTTP = true
	}
	return repo, nil
}

func (rc *RepoClient) Push(ctx context.Context, dest string) error {
	cfg := rc.Cfg
	ll := log.With().Str("config", cfg.Filename).Str("package", cfg.Package).Logger()

	base := filepath.Dir(cfg.Filename)

	fs, err := file.New(base)
	if err != nil {
		return err
	}
	defer fs.Close()

	srcs, err := resolveSrcs(cfg)
	if err != nil {
		return err
	}
	if len(srcs) == 0 {
		return fmt.Errorf("no srcs found to push")
	}
	pkgSrcs, err := resolveFilesInPkgs(cfg)
	if err != nil {
		return err
	}
	srcs = append(srcs, pkgSrcs...)

	fds := make([]v1.Descriptor, 0)
	for _, src := range srcs {
		ll.Trace().Str("file", src).Msg("adding source to upload")
		mimetype := mime.TypeByExtension(filepath.Ext(src))
		fd, err := fs.Add(ctx, src, mimetype, "")
		if err != nil {
			return err
		}

		fds = append(fds, fd)
	}

	mediaType := "application/starkpkg"
	md, err := oras.Pack(ctx, fs, mediaType, fds, oras.PackOptions{
		PackImageManifest: true,
	})
	if err != nil {
		return err
	}

	localTag := "latest"
	err = fs.Tag(ctx, md, localTag)
	if err != nil {
		return err
	}

	repo, err := rc.getRepository(dest)
	if err != nil {
		return err
	}

	ll.Debug().Str("repo", dest).Msg("uploading to repo")
	_, err = oras.Copy(
		ctx, fs, localTag, repo, repo.Reference.Reference, oras.DefaultCopyOptions)
	return err
}

func (rc *RepoClient) Pull(ctx context.Context, src string, name string) error {
	cfg := rc.Cfg
	ll := log.With().Str("config", cfg.Filename).Str("package", cfg.Package).Logger()

	fs, err := file.New(cfg.DependencyPath(name))
	if err != nil {
		return err
	}
	defer fs.Close()

	repo, err := rc.getRepository(src)
	if err != nil {
		return err
	}

	ll.Debug().Str("module", name).Str("repo", src).Msg("downloading module")
	_, err = oras.Copy(ctx, repo, repo.Reference.Reference, fs, "latest", oras.DefaultCopyOptions)
	return err
}
