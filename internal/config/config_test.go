package config_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/spruett/stark/internal/config"
)

func TestLoadConfig(t *testing.T) {
	cfg, err := config.LoadCfg("./test_data/starkcfg1.star")
	require.Nil(t, err)
	require.Equal(t, cfg.Package, "starkcfg1")
	require.Len(t, cfg.Deps, 1)
	require.Len(t, cfg.Srcs, 2)

	cfg, err = config.LoadCfg("./test_data/starkcfg2.star")
	require.Nil(t, err)
	require.Equal(t, cfg.Package, "starkcfg2")
	require.Len(t, cfg.Deps, 0)
	require.Len(t, cfg.Srcs, 0)


	_, err = config.LoadCfg("./test_data/starkcfg3.star")
	require.Error(t, err)
}
