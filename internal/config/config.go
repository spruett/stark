package config

import (
	"fmt"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"gitlab.com/spruett/stark/internal/eval/builtins"
	"go.starlark.net/starlark"
)

type StarkCfg struct {
	Filename string

	Package string
	Srcs    []string

	Deps map[string]string
}

func (cfg *StarkCfg) DependencyPath(depName string) string {
	base := filepath.Dir(cfg.Filename)
	return filepath.Join(base, DependencyDir, depName)
}

func depsFromValue(val starlark.Value) (map[string]string, error) {
	dict, ok := val.(*starlark.Dict)
	if !ok {
		return nil, fmt.Errorf("starkcfg.star deps is not a dict, instead is %s", val.Type())
	}
	deps := make(map[string]string)
	for _, depNameRaw := range dict.Keys() {
		depName, ok := depNameRaw.(starlark.String)
		if !ok {
			return nil, fmt.Errorf("starkcfg.star deps has non-string key %s", depNameRaw.String())
		}
		valueRaw, ok, err := dict.Get(depName)
		if err != nil {
			return nil, fmt.Errorf("starkcfg.star internal deps error: %w", err)
		}
		if !ok {
			return nil, fmt.Errorf("starkcfg.star internal deps error: key not found")
		}
		valueString, ok := valueRaw.(starlark.String)
		if !ok {
			return nil, fmt.Errorf("starkcfg.star deps has non-string value %s", valueRaw.String())
		}
		deps[depName.GoString()] = valueString.GoString()
	}
	return deps, nil
}

func srcsFromValue(val starlark.Value) ([]string, error) {
	list, ok := val.(*starlark.List)
	if !ok {
		return nil, fmt.Errorf("starkcfg.star srcs is not a list, instead is %s", val.Type())
	}
	srcs := make([]string, 0)
	for i := 0; i < list.Len(); i++ {
		itemStr, ok := list.Index(i).(starlark.String)
		if !ok {
			return nil, fmt.Errorf("starkcfg.star srcs has non-string value: %v", list.Index(i))
		}
		srcs = append(srcs, itemStr.GoString())
	}
	return srcs, nil
}

func cfgFromDict(dict starlark.StringDict) (*StarkCfg, error) {
	cfg := StarkCfg{}
	packageRaw, ok := dict["package"]
	if !ok {
		return nil, fmt.Errorf("starkcfg.star missing package name")
	}
	packageString, ok := packageRaw.(starlark.String)
	if !ok {
		return nil, fmt.Errorf("starkcfg.star package name is not a string, instead is %s", packageRaw.Type())
	}
	cfg.Package = packageString.GoString()

	depsRaw, ok := dict["deps"]
	if ok {
		deps, err := depsFromValue(depsRaw)
		if err != nil {
			return nil, err
		}
		cfg.Deps = deps
	} else {
		cfg.Deps = make(map[string]string)
	}

	srcsRaw, ok := dict["srcs"]
	if ok {
		srcs, err := srcsFromValue(srcsRaw)
		if err != nil {
			return nil, err
		}
		cfg.Srcs = srcs
	} else {
		cfg.Srcs = make([]string, 0)
	}

	return &cfg, nil
}

func LoadCfg(filename string) (*StarkCfg, error) {
	log.Trace().Str("file", filename).Msg("loading starkcfg")

	thread := starlark.Thread{Name: filename}
	vars := builtins.BaseBuiltins()
	vals, err := starlark.ExecFile(&thread, filename, nil, vars)
	if err != nil {
		return nil, fmt.Errorf("starkcfg.star failed to load: %w", err)
	}
	cfg, err := cfgFromDict(vals)
	if err != nil {
		return nil, err
	}
	cfg.Filename = filename
	return cfg, nil
}
