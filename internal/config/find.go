package config

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
)

var ConfigFile string = "starkcfg.star"
var DependencyDir string = filepath.Join(".stark", "pkgs")

func isPackageRoot(dir string) bool {
	cfgPath := filepath.Join(dir, ConfigFile)
	cfgStat, err := os.Stat(cfgPath)
	if err == nil && cfgStat != nil {
		return true
	}

	gitDir := filepath.Join(dir, ".git")
	gitStat, err := os.Stat(gitDir)
	return err == nil && gitStat.IsDir()
}

func FindPackageRoot(relativeTo string) (string, error) {
	path, err := filepath.Abs(relativeTo)
	if err != nil {
		return "", err
	}
	stat, _ := os.Stat(path)
	if stat != nil && stat.IsDir() && isPackageRoot(path) {
		return path, nil
	}

	for {
		parent := filepath.Dir(path)
		if parent == path {
			break
		}

		if isPackageRoot(parent) {
			return parent, nil
		}
		path = parent
	}
	return "", fmt.Errorf("no package root found for file: %s", relativeTo)
}

func FindAndLoadConfig(relativeTo string) (*StarkCfg, error) {
	packageRoot, err := FindPackageRoot(relativeTo)
	if err != nil {
		return nil, err
	}
	log.Trace().Str("root", packageRoot).Msg("found package root")

	cfgPath := filepath.Join(packageRoot, ConfigFile)
	cfgStat, err := os.Stat(cfgPath)
	if err != nil || cfgStat == nil {
		log.Trace().Str("path", cfgPath).Msg("no config found")
		return nil, nil
	}

	return LoadCfg(cfgPath)
}
