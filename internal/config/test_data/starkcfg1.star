package = "starkcfg1"

deps = {
    "kube": "package.io/stark/kube:3.12"
}

srcs = [
    "./test/test.star",
    "./glob/*.star",
]
