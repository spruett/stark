package kube

import (
	"context"
	"fmt"
	"os"
	"path"

	"github.com/rs/zerolog/log"
	v1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

func getClient(cfg KubeCfg, target Target) (*kubernetes.Clientset, error) {
	kubeconfig := path.Join(homedir.HomeDir(), ".kube", "config")
	if cfg.Kubeconfig != "" {
		kubeconfig = cfg.Kubeconfig
	} else if kubeconfigEnv := os.Getenv("KUBECONFIG"); kubeconfigEnv != "" {
		kubeconfig = kubeconfigEnv
	}
	config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeconfig},
		&clientcmd.ConfigOverrides{CurrentContext: target.Cluster},
	).ClientConfig()
	if err != nil {
		return nil, err
	}

	return kubernetes.NewForConfig(config)
}

func toLabelSelector(l map[string]string) (string, error) {
	sel := labels.NewSelector()
	for k, v := range l {
		req, err := labels.NewRequirement(k, selection.Equals, []string{v})
		if err != nil {
			return "", err
		}
		sel = sel.Add(*req)
	}
	return sel.String(), nil
}

type DeploymentStatus struct {
	Kind      string
	Name      string
	Namespace string

	UpdatedPods int
	DesiredPods int

	TotalPods int
	ReadyPods int

	Err  error
	Done bool
}

func (ds *DeploymentStatus) FullName() string {
	return fmt.Sprintf("%s %s.%s", ds.Kind, ds.Namespace, ds.Name)
}

func AllStatusesDone(deploys map[string]*DeploymentStatus) bool {
	for _, ds := range deploys {
		if !ds.Done {
			return false
		}
	}
	return true
}

func deploymentToStatus(deploy *v1.Deployment) DeploymentStatus {
	status := DeploymentStatus{
		Kind:      "Deployment",
		Name:      deploy.Name,
		Namespace: deploy.Namespace,

		UpdatedPods: int(deploy.Status.UpdatedReplicas),
		DesiredPods: int(*deploy.Spec.Replicas),

		ReadyPods: int(deploy.Status.AvailableReplicas),
		TotalPods: int(deploy.Status.Replicas),
	}
	status.Done = status.UpdatedPods == status.DesiredPods &&
		status.ReadyPods == status.TotalPods &&
		status.ReadyPods == status.DesiredPods
	return status
}

func statefulSetToStatus(ss *v1.StatefulSet) DeploymentStatus {
	status := DeploymentStatus{
		Kind:      "StatefulSet",
		Name:      ss.Name,
		Namespace: ss.Namespace,

		UpdatedPods: int(ss.Status.UpdatedReplicas),
		DesiredPods: int(*ss.Spec.Replicas),

		ReadyPods: int(ss.Status.AvailableReplicas),
		TotalPods: int(ss.Status.Replicas),
	}
	status.Done = status.UpdatedPods == status.DesiredPods &&
		status.ReadyPods == status.TotalPods &&
		status.ReadyPods == status.DesiredPods
	return status
}

func daemonSetToStatus(ds *v1.DaemonSet) DeploymentStatus {
	status := DeploymentStatus{
		Kind:      "DaemonSet",
		Name:      ds.Name,
		Namespace: ds.Namespace,

		UpdatedPods: int(ds.Status.UpdatedNumberScheduled),
		DesiredPods: int(ds.Status.DesiredNumberScheduled),

		ReadyPods: int(ds.Status.NumberReady),
		TotalPods: int(ds.Status.CurrentNumberScheduled),
	}
	status.Done = status.UpdatedPods == status.DesiredPods &&
		status.ReadyPods == status.TotalPods &&
		status.ReadyPods == status.DesiredPods
	return status
}

func watchDeployable[T any, PT any](
	ctx context.Context,
	cfg KubeCfg,
	watchFn func() (watch.Interface, error),
	mapWatchFn func(PT) DeploymentStatus,
	listFn func() ([]T, error),
	mapListFn func(*T) DeploymentStatus,
) ([]DeploymentStatus, chan DeploymentStatus, error) {
	w, err := watchFn()
	if err != nil {
		return nil, nil, err
	}
	list, err := listFn()
	if err != nil {
		return nil, nil, err
	}

	ch := make(chan DeploymentStatus)
	statuses := make([]DeploymentStatus, 0)
	for _, d := range list {
		statuses = append(statuses, mapListFn(&d))
	}

	go func() {
		watchCh := w.ResultChan()
		defer close(ch)

		currentStatuses := make(map[string]*DeploymentStatus)
		for _, status := range statuses {
			currentStatuses[status.FullName()] = &status
		}
	loop:
		for {
			select {
			case <-ctx.Done():
				break loop
			case ev, ok := <-watchCh:
				if !ok {
					ch <- DeploymentStatus{
						Err:  fmt.Errorf("watch closed"),
						Done: true,
					}
					break loop
				}

				deploy := ev.Object.(PT)
				status := mapWatchFn(deploy)
				ch <- status

				currentStatuses[status.FullName()] = &status
				if AllStatusesDone(currentStatuses) {
					break loop
				}
			}
		}
	}()
	return statuses, ch, nil
}

func mergeWatches(cs ...<-chan DeploymentStatus) chan DeploymentStatus {
	out := make(chan DeploymentStatus)

	for _, c := range cs {
		go func(ch <-chan DeploymentStatus) {
			for v := range ch {
				out <- v
			}
		}(c)
	}
	return out
}

func WatchDeployments(
	ctx context.Context,
	labels map[string]string,
	cfg KubeCfg,
	target Target,
) ([]DeploymentStatus, chan DeploymentStatus, error) {
	client, err := getClient(cfg, target)
	if err != nil {
		return nil, nil, err
	}

	selector, err := toLabelSelector(labels)
	if err != nil {
		return nil, nil, err
	}
	log.Debug().Str("selector", selector).Msg("watching deployments")

	statuses := make([]DeploymentStatus, 0)

	deploys, deployWatch, err := watchDeployable(
		ctx,
		cfg,
		func() (watch.Interface, error) {
			return client.AppsV1().Deployments(target.Namespace).
				Watch(ctx, apiv1.ListOptions{
					LabelSelector: selector,
				})
		},
		deploymentToStatus,
		func() ([]v1.Deployment, error) {
			deploys, err := client.AppsV1().Deployments(target.Namespace).
				List(ctx, apiv1.ListOptions{
					LabelSelector: selector,
				})
			if err != nil {
				return nil, err
			}
			return deploys.Items, nil
		},
		deploymentToStatus,
	)
	if err != nil {
		return nil, nil, err
	}
	statefulSets, statefulSetWatch, err := watchDeployable(
		ctx,
		cfg,
		func() (watch.Interface, error) {
			return client.AppsV1().StatefulSets(target.Namespace).
				Watch(ctx, apiv1.ListOptions{
					LabelSelector: selector,
				})
		},
		statefulSetToStatus,
		func() ([]v1.StatefulSet, error) {
			sets, err := client.AppsV1().StatefulSets(target.Namespace).
				List(ctx, apiv1.ListOptions{
					LabelSelector: selector,
				})
			if err != nil {
				return nil, err
			}
			return sets.Items, nil
		},
		statefulSetToStatus,
	)
	if err != nil {
		return nil, nil, err
	}

	daemonSets, daemonSetWatch, err := watchDeployable(
		ctx,
		cfg,
		func() (watch.Interface, error) {
			return client.AppsV1().DaemonSets(target.Namespace).
				Watch(ctx, apiv1.ListOptions{
					LabelSelector: selector,
				})
		},
		daemonSetToStatus,
		func() ([]v1.DaemonSet, error) {
			sets, err := client.AppsV1().DaemonSets(target.Namespace).
				List(ctx, apiv1.ListOptions{
					LabelSelector: selector,
				})
			if err != nil {
				return nil, err
			}
			return sets.Items, nil
		},
		daemonSetToStatus,
	)
	if err != nil {
		return nil, nil, err
	}

	statuses = append(statuses, deploys...)
	statuses = append(statuses, statefulSets...)
	statuses = append(statuses, daemonSets...)
	ch := mergeWatches(deployWatch, statefulSetWatch, daemonSetWatch)
	return statuses, ch, nil
}
