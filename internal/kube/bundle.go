package kube

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"

	"gopkg.in/yaml.v3"
)

var StarkBundle string = "StarkBundle"

type Target struct {
	Namespace string `yaml:"namespace,omitempty"`
	Cluster   string `yaml:"cluster,omitempty"`
}

type Metadata struct {
	Name      string `yaml:"name"`
	Namespace string `yaml:"namespace"`
}

type Bundle struct {
	Kind      string            `yaml:"kind,omitempty"`
	Metadata  Metadata          `yaml:"metadata"`
	Target    Target            `yaml:"target,omitempty"`
	Labels    map[string]string `yaml:"labels,omitempty"`
	Resources []yaml.Node       `yaml:"resources,omitempty"`
}

type ResourceBase struct {
	APIVersion string   `yaml:"apiVersion,omitempty"`
	Kind       string   `yaml:"kind"`
	Metadata   Metadata `yaml:"metadata"`
}

func BundlesFromYaml(encoded string) ([]Bundle, error) {
	decoder := yaml.NewDecoder(bytes.NewReader([]byte(encoded)))
	bundles := make([]Bundle, 0)

	for {
		var b Bundle
		err := decoder.Decode(&b)
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return nil, fmt.Errorf("failed to decode bundle: %v", err)
		}

		if b.Kind != StarkBundle {
			if len(bundles) > 0 {
				return nil, fmt.Errorf("unexpected mix of bundles and non-bundle")
			} else {
				return nil, fmt.Errorf("not a bundle")
			}
		}

		bundles = append(bundles, b)
	}
	return bundles, nil
}

func (b *Bundle) ResourcesYaml() (string, error) {
	buf := new(strings.Builder)
	for i, r := range b.Resources {
		bytes, err := yaml.Marshal(r)
		if err != nil {
			return "", err
		}
		buf.Write(bytes)
		if i != len(b.Resources)-1 {
			buf.WriteString("---\n")
		}
	}
	return buf.String(), nil
}

func (b *Bundle) ResourcesTyped() ([]ResourceBase, error) {
	rs := make([]ResourceBase, 0, len(b.Resources))
	for _, r := range b.Resources {
		var rb ResourceBase
		err := r.Decode(&rb)
		if err != nil {
			return nil, err
		}
		rs = append(rs, rb)
	}
	return rs, nil
}
