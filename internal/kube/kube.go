package kube

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/rs/zerolog/log"
)

type KubeCfg struct {
	Kubeconfig string
	Kubectl    string
	DiffCmd    string
}

type KubectlOpts struct {
	Cfg KubeCfg

	Command string
	Args    []string
	Input   string

	Target Target

	Env map[string]string
}

func (opts *KubectlOpts) PrettyDiff(args ...string) {
	if opts.Env == nil {
		opts.Env = make(map[string]string)
	}
	opts.Env["KUBECTL_EXTERNAL_DIFF"] = fmt.Sprintf("%s external-diff %s", os.Args[0], strings.Join(args, " "))
}
func (opts *KubectlOpts) ExternalDiffCmd(cmd string) {
	if opts.Env == nil {
		opts.Env = make(map[string]string)
	}
	opts.Env["KUBECTL_EXTERNAL_DIFF"] = cmd
}

func RunKubectl(command string, args []string, input string) error {
	return RunKubectlWith(&KubectlOpts{
		Command: command,
		Args:    args,
		Input:   input,
	})
}

func RunKubectlWith(opts *KubectlOpts) error {
	fullArgs := make([]string, 0)
	if opts.Target.Cluster != "" {
		fullArgs = append(fullArgs, "--context", opts.Target.Cluster)
	}
	if opts.Target.Namespace != "" {
		fullArgs = append(fullArgs, "--namespace", opts.Target.Namespace)
	}

	fullArgs = append(fullArgs, opts.Command)
	fullArgs = append(fullArgs, opts.Args...)

	binary := "kubectl"
	if opts.Cfg.Kubectl != "" {
		binary = opts.Cfg.Kubectl
	}

	log.Debug().Str("kubectl", binary).Interface("env", opts.Env).Strs("args", fullArgs).Msg("executing kubectl")

	cmd := exec.Command("kubectl", fullArgs...)
	cmd.Env = append(cmd.Env, os.Environ()...)
	if opts.Cfg.Kubeconfig != "" {
		cmd.Env = append(cmd.Env, "KUBECONFIG="+opts.Cfg.Kubeconfig)
	}
	for k, v := range opts.Env {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
	}
	cmd.Stdin = strings.NewReader(opts.Input)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
