package commands

import (
	"fmt"
	"os/exec"

	"gitlab.com/spruett/stark/internal/encoding"
	"gitlab.com/spruett/stark/internal/eval"
	"go.starlark.net/starlark"
)

type OutputFormat string

const (
	Json OutputFormat = "json"
	Yaml OutputFormat = "yaml"
)

type ContextArgs struct {
	Version string            `help:"Version of the image to apply, set in ctx['version']"`
	Context map[string]string `help:"Generic values passed to Starlark evaluation in ctx"`
}

func (c *ContextArgs) ToStarlark() *starlark.Dict {
	sd := starlark.NewDict(len(c.Context) + 1)
	for k, v := range c.Context {
		sd.SetKey(starlark.String(k), starlark.String(v))
	}
	if c.Version != "" {
		sd.SetKey(starlark.String("version"), starlark.String(c.Version))
	}
	return sd
}

type FileArgs struct {
	Files []string `arg:"" help:"Path to stark config file" type:"existingfile"`
}

type Cli struct {
	Debug bool `short:"v" help:"Set log level to DEBUG for more information"`
	Trace bool `help:"Set log level to TRACE for even more information"`

	Build BuildCmd    `cmd:"" help:"Build a stark config and output YAML"`
	Fmt   FmtCmd      `cmd:"" aliases:"format" help:"Format a stark config"`
	Gen   GenerateCmd `cmd:"" aliases:"generate" help:"Generate files from stark configs"`
	Kube  KubeCmd     `cmd:"" aliases:"k" help:"Work with exported Stark bundles in Kubernetes"`

	Pkg PkgCmd `cmd:"" aliases:"p" help:"Work with packages and dependencies"`

	RawKubectl   KubectlCmd      `cmd:"" hidden:"" help:"Manage raw Kubernetes resources without Bundles"`
	ExternalDiff ExternalDiffCmd `cmd:"" hidden:""`
}

func getEncoder(format OutputFormat, env *eval.Env) (encoding.Encoder, error) {
	if format == Json {
		return &encoding.JsonEncoder{Env: env}, nil
	} else if format == Yaml {
		return &encoding.YamlEncoder{Env: env}, nil
	}
	return nil, fmt.Errorf("unsupported output format: %v", format)
}

func evalFiles(files []string, ctx *ContextArgs, fmt OutputFormat) (string, error) {
	env := eval.NewEnv()
	env.AddCtx(ctx.ToStarlark())
	encoder, err := getEncoder(fmt, env)
	if err != nil {
		return "", err
	}

	values := make([]starlark.Value, 0)
	for _, file := range files {
		vs, err := env.EvalFile(file)
		if err != nil {
			return "", err
		}
		values = append(values, vs...)
	}

	yaml, err := encoder.EncodeMany(values)
	if err != nil {
		return "", err
	}
	return yaml, nil
}

func isExitError(err error) bool {
	if err == nil {
		return false
	}

	_, ok := err.(*exec.ExitError)
	return ok
}
