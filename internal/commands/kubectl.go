package commands

import (
	"os"
	"os/exec"

	"github.com/alecthomas/kong"
)

type KubectlCmd struct {
	Apply KubectlApplyCmd `cmd:"" aliases:"a" help:"Apply raw resources with kubectl apply"`
	Diff  KubectlDiffCmd  `cmd:"" help:"Diff raw resources with kubectl diff"`
}

type KubectlApplyCmd struct {
	ContextArgs
	FileArgs
	DryRun bool `help:"Run a server dry-run instead of applying"`
}

func (cmd *KubectlApplyCmd) Run(args *kong.Context) error {
	opts := make([]string, 0)
	if cmd.DryRun {
		opts = append(opts, "--dry-run=server")
	}
	return runKubectlWith("apply", opts, cmd.Files, &cmd.ContextArgs)
}

type KubectlDiffCmd struct {
	ContextArgs
	FileArgs
}

func (cmd *KubectlDiffCmd) Run(args *kong.Context) error {
	err := runKubectlWith("diff", []string{}, cmd.Files, &cmd.ContextArgs)

	if e, ok := err.(*exec.ExitError); ok {
		os.Exit(e.ExitCode())
	}
	return err
}
