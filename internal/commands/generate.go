package commands

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"path"

	"github.com/alecthomas/kong"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
)

const generatedFileKind = "GeneratedFile"

type GeneratedFile struct {
	Kind     string `yaml:"kind"`
	Filename string `yaml:"filename"`
	Data     string `yaml:"data"`
}

func generatedFilesFromYaml(encoded string) ([]GeneratedFile, error) {
	decoder := yaml.NewDecoder(bytes.NewReader([]byte(encoded)))
	files := make([]GeneratedFile, 0)

	for {
		var f GeneratedFile
		err := decoder.Decode(&f)
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return nil, fmt.Errorf("failed to decode bundle: %v", err)
		}

		if f.Kind != generatedFileKind {
			if len(files) > 0 {
				return nil, fmt.Errorf("unexpected mix of bundles and non-bundle")
			} else {
				return nil, fmt.Errorf("not a bundle")
			}
		}

		files = append(files, f)
	}
	return files, nil
}

type GenerateCmd struct {
	FileArgs
	ContextArgs

	Out string `short:"o" help:"Override output base path, defaults to relative to input file"`
}

func generateFile(dir string, gf GeneratedFile) error {
	newPath := path.Join(dir, gf.Filename)

	fd, err := os.OpenFile(newPath, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer fd.Close()

	log.Info().Str("path", newPath).Msg("generated file")
	fd.WriteString(gf.Data)
	return nil
}

func (cmd *GenerateCmd) Run(args *kong.Context) error {
	for _, filename := range cmd.Files {
		yamls, err := evalFiles([]string{filename}, &cmd.ContextArgs, Yaml)
		if err != nil {
			return err
		}
		gfs, err := generatedFilesFromYaml(yamls)
		if err != nil {
			return err
		}

		dir := path.Dir(filename)
		if cmd.Out != "" {
			dir = cmd.Out
		}

		for _, gf := range gfs {
			err = generateFile(dir, gf)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
