package commands

import "gitlab.com/spruett/stark/internal/kube"

func runKubectlWith(subcommand string, args []string, inputs []string, ctx *ContextArgs) error {
	yaml, err := evalFiles(inputs, ctx, Yaml)
	if err != nil {
		return err
	}
	args = append(args, "-f", "-")
	return kube.RunKubectl(subcommand, args, yaml)
}
