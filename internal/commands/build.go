package commands

import (
	"fmt"
	"os"

	"github.com/alecthomas/kong"
)

type BuildCmd struct {
	FileArgs
	ContextArgs

	Format OutputFormat `short:"f" help:"Output format (yaml,json)" default:"yaml"`
	Output string       `short:"o" help:"Write output to a file instead of stdout" type:"path"`
}

func (cmd *BuildCmd) Run(args *kong.Context) error {
	yaml, err := evalFiles(cmd.Files, &cmd.ContextArgs, cmd.Format)
	if err != nil {
		return err
	}

	if cmd.Output == "" {
		fmt.Print(yaml)
	} else {
		fd, err := os.OpenFile(cmd.Output, os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		defer fd.Close()

		_, err = fmt.Fprint(fd, yaml)
		if err != nil {
			return err
		}
	}
	return nil
}
