package commands

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/spruett/stark/internal/config"
	"gitlab.com/spruett/stark/internal/deps"
)

type PkgCmd struct {
	Init      PkgInitCmd      `cmd:"" aliases:"new,n" help:"Generate a starkcfg.star for the current package"`
	Push      PkgPushCmd      `cmd:"" help:"Push package to a remote repository"`
	FetchDeps PkgFetchDepsCmd `cmd:"" aliases:"fetch,fd" help:"Download all dependencies of this package"`
	Clear     PkgClearCmd     `cmd:"" help:"Delete downloaded dependencies of this package"`
}

type PkgInitCmd struct {
	Package string `arg:"" help:"Name of the package to create"`
}

func (cmd *PkgInitCmd) Run() error {
	stat, _ := os.Stat("starkcfg.star")
	if stat != nil {
		return fmt.Errorf("starkcfg.star already exists")
	}
	cfgContent := `package = "%s"

srcs = ["*.star"]

deps = {}
`
	fd, err := os.OpenFile("starkcfg.star", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	_, err = io.WriteString(fd, fmt.Sprintf(cfgContent, cmd.Package))
	if err != nil {
		return err
	}
	return nil
}

type PkgPushCmd struct {
	Destination string `arg:"" help:"Remote reference to push to"`
}

func getRepoClient() (*deps.RepoClient, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	cfg, err := config.FindAndLoadConfig(wd)
	if err != nil {
		return nil, err
	}
	if cfg == nil {
		return nil, fmt.Errorf("no starkcfg.star")
	}

	cli := deps.NewRepoClient(cfg)
	return cli, nil
}

func (cmd *PkgPushCmd) Run() error {
	cli, err := getRepoClient()
	if err != nil {
		return err
	}
	err = cli.Push(context.TODO(), cmd.Destination)
	if err != nil {
		return err
	}
	fmt.Println("pushed to " + cmd.Destination)
	return nil
}

type PkgFetchDepsCmd struct{}

func (cmd *PkgFetchDepsCmd) Run() error {
	cli, err := getRepoClient()
	if err != nil {
		return err
	}

	if cli.Cfg == nil || len(cli.Cfg.Deps) == 0 {
		fmt.Println("no dependencies in starkcfg.star")
		return nil
	}

	for name, dep := range cli.Cfg.Deps {
		err = cli.Pull(context.TODO(), dep, name)
		if err != nil {
			return err
		}
		fmt.Printf("downloaded %s as %s\n", dep, name)
	}
	return nil
}

type PkgClearCmd struct{}

func (cmd *PkgClearCmd) Run() error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	cfg, err := config.FindAndLoadConfig(wd)
	if err != nil {
		return err
	}
	starkDir := filepath.Join(
		filepath.Dir(cfg.Filename),
		config.DependencyDir,
	)
	return os.RemoveAll(starkDir)
}
