package commands

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"sort"
	"time"

	"gitlab.com/spruett/stark/internal/kube"

	"github.com/alecthomas/kong"
	"github.com/fatih/color"
	"github.com/gosuri/uilive"
	"github.com/manifoldco/promptui"
	"github.com/rodaine/table"
)

const ManagedByStark = "app.kubernetes.io/managed-by=stark"

type KubeCfgArgs struct {
	Kubectl    string `help:"Path to kubectl CLI"`
	Kubeconfig string `help:"Path to kubernetes config (default: ~/.kube/config)"`
	DiffCmd    string `help:"Command to use for external diff (default: pretty based on dyff)"`
}

func (args *KubeCfgArgs) ToCfg() kube.KubeCfg {
	return kube.KubeCfg{
		Kubectl:    args.Kubectl,
		Kubeconfig: args.Kubeconfig,
		DiffCmd:    args.DiffCmd,
	}
}

type KubeCmd struct {
	List   KubeListCmd   `cmd:"" aliases:"ls" help:"List resources in a Kubernetes bundle"`
	Diff   KubeDiffCmd   `cmd:"" help:"Diff resources in Kubernetes bundle with resources in cluster"`
	Deploy KubeDeployCmd `cmd:"" aliases:"d" help:"Deploy resources in a Kubernetes bundle to the target cluster"`
}

type KubeListCmd struct {
	ContextArgs
	FileArgs
}

func evalFilesToBundles(filenames []string, context *ContextArgs) ([]kube.Bundle, error) {
	yaml, err := evalFiles(filenames, context, Yaml)
	if err != nil {
		return nil, err
	}

	bs, err := kube.BundlesFromYaml(yaml)
	if err != nil {
		return nil, fmt.Errorf("failed to parse bundle: use export(bundle(...)): %v", err)
	}
	return bs, err
}

func (cmd *KubeListCmd) Run(args *kong.Context) error {
	bs, err := evalFilesToBundles(cmd.Files, &cmd.ContextArgs)
	if err != nil {
		return err
	}

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()

	tbl := table.New("Bundle", "Kind", "Namespace", "Name")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	for _, b := range bs {
		rs, err := b.ResourcesTyped()
		if err != nil {
			return err
		}

		for _, resource := range rs {
			namespace := resource.Metadata.Namespace
			if namespace == "" {
				namespace = "default"
			}

			tbl.AddRow(b.Metadata.Name, resource.Kind, namespace, resource.Metadata.Name)
		}
	}
	tbl.Print()
	return nil
}

type KubeDiffCmd struct {
	KubeCfgArgs

	ContextArgs
	FileArgs
}

func getBundleLabels(b *kube.Bundle) map[string]string {
	const ManagedBy = "app.kubernetes.io/managed-by"
	const PartOf = "app.kubernetes.io/part-of"
	return map[string]string{
		ManagedBy: "stark",
		PartOf:    b.Metadata.Name,
	}
}

func getPruneArgs(b *kube.Bundle) []string {
	labels := getBundleLabels(b)
	labelStrings := make([]string, 0, len(labels))
	for k, v := range labels {
		labelStrings = append(labelStrings, fmt.Sprintf("%s=%s", k, v))
	}
	return labelStrings
}

func bundleDiff(bundles []kube.Bundle, cfg kube.KubeCfg) error {
	var overallErr error
	for _, bundle := range bundles {
		yaml, err := bundle.ResourcesYaml()
		if err != nil {
			return err
		}
		args := []string{"-f", "-"}

		kopts := kube.KubectlOpts{
			Cfg:     cfg,
			Command: "diff",
			Input:   yaml,
			Args:    args,
			Target:  bundle.Target,
		}
		if cfg.DiffCmd != "" {
			kopts.ExternalDiffCmd(cfg.DiffCmd)
		} else {

			kopts.PrettyDiff()
		}
		err = kube.RunKubectlWith(&kopts)
		if isExitError(err) {
			overallErr = err
		} else if err != nil {
			return err
		}
	}
	return overallErr
}

func (cmd *KubeDiffCmd) Run(args *kong.Context) error {
	bs, err := evalFilesToBundles(cmd.Files, &cmd.ContextArgs)
	if err != nil {
		return err
	}
	err = bundleDiff(bs, cmd.ToCfg())
	if e, ok := err.(*exec.ExitError); ok {
		os.Exit(e.ExitCode())
	}
	return err
}

type KubeDeployCmd struct {
	KubeCfgArgs
	FileArgs
	ContextArgs

	Watch          bool `short:"w" negatable:"" default:"false" help:"Watch code resources (Deployments) be applied"`
	DryRun         bool `help:"Run a dry-run instead of applying resources"`
	NonInteractive bool `short:"y" help:"Don't prompt, assume yes and deploy"`

	// TODO: use pruning as the default, after kubectl diff --prune is fixed
	Delete bool `help:"Prune old resources which no longer exist in the bundle while deploying"`
}

func sortDeploys(deploys map[string]*kube.DeploymentStatus) []*kube.DeploymentStatus {
	ds := make([]*kube.DeploymentStatus, 0, len(deploys))
	for _, d := range deploys {
		ds = append(ds, d)
	}
	sort.Slice(ds, func(i, j int) bool {
		return ds[i].FullName() < ds[j].FullName()
	})
	return ds
}

func (cmd *KubeDeployCmd) displayDeployStatus(
	frame int,
	bundle *kube.Bundle,
	deploys map[string]*kube.DeploymentStatus,
	writer *uilive.Writer,
) {
	boldGreen := color.New(color.FgGreen, color.Bold).SprintFunc()
	boldYellow := color.New(color.FgYellow, color.Bold).SprintFunc()

	cyan := color.New(color.FgCyan).SprintFunc()
	boldCyan := color.New(color.FgCyan, color.Bold).SprintFunc()
	frames := []string{"⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"}

	writer.Start()

	if kube.AllStatusesDone(deploys) {
		fmt.Fprintf(writer, "Bundle %s: %s\n", bundle.Metadata.Name, boldGreen("complete"))
	} else {
		fmt.Fprintf(writer, "Bundle %s: %s...\n", bundle.Metadata.Name, boldCyan("updating"))
	}

	ds := sortDeploys(deploys)
	for _, deploy := range ds {
		status := boldCyan(frames[frame%len(frames)])
		colorFn := cyan
		if deploy.Done {
			status = boldGreen("⠿")
			colorFn = boldGreen
		}
		unreadyPods := deploy.TotalPods - deploy.ReadyPods
		unreadyMessage := ""
		if unreadyPods > 0 {
			unreadyMessage = fmt.Sprintf(" (%d %s)", unreadyPods, boldYellow("not ready"))
		}
		fmt.Fprintf(
			writer,
			"%s %s %s.%s: %d/%d pods up-to-date%s\n",
			status,
			deploy.Kind,
			colorFn(deploy.Namespace),
			colorFn(deploy.Name),
			deploy.UpdatedPods,
			deploy.DesiredPods,
			unreadyMessage,
		)
	}
	writer.Stop()
}

func (cmd *KubeDeployCmd) watchBundleDeploy(bundle *kube.Bundle) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	statuses, ch, err := kube.WatchDeployments(
		ctx,
		getBundleLabels(bundle),
		cmd.ToCfg(),
		bundle.Target,
	)
	if err != nil {
		return err
	}

	fmt.Println()
	currentStatus := make(map[string]*kube.DeploymentStatus)
	writer := uilive.New()
	for _, d := range statuses {
		currentStatus[d.FullName()] = &d
	}
	cmd.displayDeployStatus(0, bundle, currentStatus, writer)

loop:
	for i := 1; ; i++ {
		cmd.displayDeployStatus(i, bundle, currentStatus, writer)
		if kube.AllStatusesDone(currentStatus) {
			break
		}
		select {
		case <-ctx.Done():
			break loop
		case update := <-ch:
			currentStatus[update.FullName()] = &update
		case <-time.After(100 * time.Millisecond):
		}
	}
	return nil
}

func (cmd *KubeDeployCmd) bundleApply(bundles []kube.Bundle) error {
	for _, bundle := range bundles {
		kargs := []string{"-f", "-"}
		if cmd.DryRun {
			kargs = append(kargs, "--dry-run=server")
		}
		if cmd.Delete {
			kargs = append(kargs, getPruneArgs(&bundle)...)
		}
		yaml, err := bundle.ResourcesYaml()
		if err != nil {
			return err
		}
		kopts := kube.KubectlOpts{
			Cfg:     cmd.ToCfg(),
			Command: "apply",
			Args:    kargs,
			Input:   yaml,
			Target:  bundle.Target,
		}
		err = kube.RunKubectlWith(&kopts)
		if err != nil {
			return err
		}
		if cmd.Watch {
			err = cmd.watchBundleDeploy(&bundle)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (cmd *KubeDeployCmd) Run(args *kong.Context) error {
	bs, err := evalFilesToBundles(cmd.Files, &cmd.ContextArgs)
	if err != nil {
		return err
	}

	err = bundleDiff(bs, cmd.ToCfg())
	// TOOD: once kubectl diff --prune is supported, can handle deletes better
	if err == nil && !cmd.Delete {
		// No diff, otherwise would've exited non-zero
		fmt.Println("✅ No changes to deploy")
		return nil
	}
	if err != nil && !isExitError(err) {
		return err
	}

	if !cmd.NonInteractive && !cmd.DryRun {
		prompt := promptui.Prompt{
			Label:     "Deploy changes",
			IsConfirm: true,
			Default:   "Y",
		}
		_, err := prompt.Run()
		if err != nil {
			return nil
		}
	}
	return cmd.bundleApply(bs)
}
