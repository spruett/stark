package commands

import (
	"fmt"
	"os"
	"strings"

	"github.com/gonvenience/wrap"
	"github.com/gonvenience/ytbx"
	"github.com/homeport/dyff/pkg/dyff"
)

type ExternalDiffCmd struct {
	FromPath string `arg:""`
	ToPath   string `arg:""`

	IgnoreOrderChanges bool `default:"true"`
	Kubernetes         bool `default:"true"`

	Ignore []string `default:"metadata.managedFields.*"`

	Style string `default:"human"`
}

func (cmd *ExternalDiffCmd) Run() error {
	from, to, err := ytbx.LoadFiles(cmd.FromPath, cmd.ToPath)
	if err != nil {
		return wrap.Errorf(err, "failed to load input files")
	}

	report, err := dyff.CompareInputFiles(from, to,
		dyff.IgnoreOrderChanges(cmd.IgnoreOrderChanges),
		dyff.KubernetesEntityDetection(cmd.Kubernetes),
	)

	if err != nil {
		return wrap.Errorf(err, "failed to compare input files")
	}

	if cmd.Ignore != nil {
		report = report.ExcludeRegexp(cmd.Ignore...)
	}
	err = writeReport(cmd, report)
	if err != nil {
		return err
	}
	if len(report.Diffs) > 0 {
		os.Exit(1)
	}
	return nil
}

func writeReport(cmd *ExternalDiffCmd, report dyff.Report) error {
	var reportWriter dyff.ReportWriter
	switch strings.ToLower(cmd.Style) {
	case "human", "bosh":
		reportWriter = &dyff.HumanReport{
			Report:               report,
			OmitHeader:           true,
			MinorChangeThreshold: 0.1,
		}

	case "brief", "short", "summary":
		reportWriter = &dyff.BriefReport{
			Report: report,
		}

	default:
		return fmt.Errorf("unknown output style %s", cmd.Style)
	}

	if err := reportWriter.WriteReport(os.Stdout); err != nil {
		return wrap.Errorf(err, "failed to print report")
	}
	return nil
}
