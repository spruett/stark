package commands

import (
	"fmt"
	"io"
	"os"

	"github.com/alecthomas/kong"
	"github.com/bazelbuild/buildtools/build"
	"github.com/bazelbuild/buildtools/buildifier/utils"
)

type FmtCmd struct {
	FileArgs

	InPlace bool `short:"i" help:"Overwrite file instead of printing"`
}

func (cmd *FmtCmd) Run(args *kong.Context) error {
	for _, filename := range cmd.Files {
		fd, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer fd.Close()
		data, err := io.ReadAll(fd)
		if err != nil {
			return err
		}

		parser := utils.GetParser("default")
		ast, err := parser(filename, data)
		if err != nil {
			return err
		}
		formatted := string(build.Format(ast))
		if cmd.InPlace {
			fd, err := os.OpenFile(filename, os.O_WRONLY, 0644)
			if err != nil {
				return err
			}
			defer fd.Close()
			_, err = io.WriteString(fd, formatted)
			if err != nil {
				return err
			}
		} else {
			fmt.Print(formatted)
		}
	}
	return nil
}
