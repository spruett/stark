package eval

import (
	"context"

	"github.com/bufbuild/protocompile"
	"github.com/stripe/skycfg/go/protomodule"
	"go.starlark.net/starlark"
	"google.golang.org/protobuf/reflect/protoregistry"
	"google.golang.org/protobuf/types/dynamicpb"
)

func loadProtoFile(filename string) (starlark.StringDict, error) {
	compiler := protocompile.Compiler{
		Resolver: &protocompile.SourceResolver{},
	}
	files, err := compiler.Compile(context.Background(), filename)
	if err != nil {
		return nil, err
	}
	types := protoregistry.Types{}
	for _, file := range files {
		for i := 0; i < file.Messages().Len(); i++ {
			err = types.RegisterMessage(dynamicpb.NewMessageType(file.Messages().Get(i)))
			if err != nil {
				return nil, err
			}
		}
		for i := 0; i < file.Enums().Len(); i++ {
			err = types.RegisterEnum(dynamicpb.NewEnumType(file.Enums().Get(i)))
			if err != nil {
				return nil, err
			}
		}
	}

	pkg := protomodule.NewProtoPackage(&types, files[0].FullName())
	vals := make(starlark.StringDict)
	for _, attr := range pkg.AttrNames() {
		val, err := pkg.Attr(attr)
		if err != nil {
			return nil, err
		}
		vals[attr] = val
	}
	return vals, nil
}
