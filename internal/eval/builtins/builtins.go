package builtins

import (
	"fmt"
	"os"
	"strings"

	"github.com/stripe/skycfg/go/assertmodule"
	"github.com/stripe/skycfg/go/hashmodule"
	"github.com/stripe/skycfg/go/urlmodule"
	"github.com/stripe/skycfg/go/yamlmodule"
	starlarkjson "go.starlark.net/lib/json"
	"go.starlark.net/starlark"
)

var ExportKey string = "exports"

func exportBuiltin(
	thread *starlark.Thread,
	fn *starlark.Builtin,
	args starlark.Tuple,
	kwargs []starlark.Tuple,
) (starlark.Value, error) {
	if len(kwargs) != 0 {
		return nil, fmt.Errorf("invalid export(): takes no kwargs")
	} else if args.Len() == 0 {
		return nil, fmt.Errorf("invalid export(): must be called with at least one argument")
	}

	exported, ok := thread.Local(ExportKey).(*[]starlark.Value)
	if !ok {
		return nil, fmt.Errorf("invalid stark setup: no exported value configured")
	}
	if len(*exported) != 0 {
		*exported = make([]starlark.Value, 0)
	}

	for i := 0; i < args.Len(); i++ {
		*exported = append(*exported, args.Index(i))
	}
	return starlark.None, nil
}

func getBuiltin(
	thread *starlark.Thread,
	fn *starlark.Builtin,
	args starlark.Tuple,
	kwargs []starlark.Tuple,
) (starlark.Value, error) {
	var val starlark.Value
	var name starlark.Value
	var def starlark.Value = starlark.None

	err := starlark.UnpackArgs("get", args, kwargs, "struct", &val, "name", &name, "default?", &def)
	if err != nil {
		return nil, fmt.Errorf("%v: %s:%d", err, thread.CallFrame(1).Name, thread.CallFrame(1).Pos.Line)
	}

	v, ok := val.(*mutStruct)
	if !ok {
		return nil, fmt.Errorf("get(): expected struct type for first argument, got %s", val.Type())
	}
	n, ok := name.(starlark.String)
	if !ok {
		return nil, fmt.Errorf("get(): expected string type for second argument, got %s", name.Type())
	}

	for _, attr := range v.AttrNames() {
		if attr == n.GoString() {
			return v.Attr(n.GoString())
		}
	}
	return def, nil
}

func structToDict(strct *mutStruct) starlark.Value {
	dict := starlark.NewDict(len(strct.vals))
	for k, v := range strct.vals {
		if nested, ok := v.(*mutStruct); ok {
			dict.SetKey(starlark.String(k), structToDict(nested))
		} else {
			dict.SetKey(starlark.String(k), v)
		}
	}
	return dict
}

func toDictBuiltin(
	thread *starlark.Thread,
	fn *starlark.Builtin,
	args starlark.Tuple,
	kwargs []starlark.Tuple,
) (starlark.Value, error) {
	var val starlark.Value
	err := starlark.UnpackArgs("to_dict", args, kwargs, "struct", &val)
	if err != nil {
		return nil, fmt.Errorf("%v: %s:%d", err, thread.CallFrame(1).Name, thread.CallFrame(1).Pos.Line)
	}
	// to_dict() on a dict is idempotent
	dict, ok := val.(*starlark.Dict)
	if ok {
		return dict, nil
	}

	strct, ok := val.(*mutStruct)
	if !ok {
		return nil, fmt.Errorf("to_dict(): expected struct, got %v", val)
	}
	return structToDict(strct), nil
}

func allowedEnvVar(name string) bool {
	return strings.HasPrefix(name, "STARK_")
}

func envBuiltin() starlark.Value {
	var env starlark.Dict
	for _, e := range os.Environ() {
		split := strings.SplitN(e, "=", 2)
		if !allowedEnvVar(split[0]) {
			continue
		}

		env.SetKey(
			starlark.String(split[0]),
			starlark.String(split[1]),
		)
	}
	return &env
}

func BaseBuiltins() starlark.StringDict {
	return starlark.StringDict{
		"export":  starlark.NewBuiltin("export", exportBuiltin),
		"fail":    assertmodule.Fail,
		"hash":    hashmodule.NewModule(),
		"json":    starlarkjson.Module,
		"struct":  starlark.NewBuiltin("struct", mutStructBuiltin),
		"get":     starlark.NewBuiltin("get", getBuiltin),
		"to_dict": starlark.NewBuiltin("to_dict", toDictBuiltin),
		"yaml":    yamlmodule.NewModule(),
		"url":     urlmodule.NewModule(),
		"env":     envBuiltin(),
	}
}
