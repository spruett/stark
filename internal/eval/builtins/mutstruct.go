package builtins

import (
	"fmt"

	"go.starlark.net/starlark"
	"go.starlark.net/syntax"
)

type mutStruct struct {
	vals   starlark.StringDict
	frozen bool
}

func (s *mutStruct) String() string {
	return s.vals.String()
}

func (s *mutStruct) Type() string {
	return "struct"
}

func (s *mutStruct) Truth() starlark.Bool {
	return true
}

func (s *mutStruct) Hash() (uint32, error) {
	if !s.frozen {
		return 0, fmt.Errorf("cannot hash unfrozen struct")
	}
	var hash uint32
	for k, v := range s.vals {
		vh, err := v.Hash()
		if err != nil {
			return 0, err
		}
		kh, _ := starlark.String(k).Hash()

		hash ^= vh
		hash ^= kh
	}
	return hash, nil
}

func (s *mutStruct) Freeze() {
	s.frozen = true
	for _, v := range s.vals {
		v.Freeze()
	}
}

func (s *mutStruct) Attr(name string) (starlark.Value, error) {
	v, ok := s.vals[name]
	if !ok {
		return nil, starlark.NoSuchAttrError(
			fmt.Sprintf("struct has no .%s attribute", name))
	}
	return v, nil
}

func (s *mutStruct) AttrNames() []string {
	names := make([]string, 0, len(s.vals))
	for k := range s.vals {
		names = append(names, k)
	}
	return names
}

func (x *mutStruct) Binary(op syntax.Token, y starlark.Value, side starlark.Side) (starlark.Value, error) {
	if y, ok := y.(*mutStruct); ok && op == syntax.PLUS {
		if side == starlark.Right {
			x, y = y, x
		}

		merged := make(starlark.StringDict)
		for k, v := range x.vals {
			merged[k] = v
		}
		for k, v := range y.vals {
			merged[k] = v
		}

		return &mutStruct{vals: merged}, nil
	}
	return nil, nil
}

func (s *mutStruct) SetField(name string, val starlark.Value) error {
	s.vals[name] = val
	return nil
}

type mutStructIterator struct {
	s    *mutStruct
	keys []string
	ix   int
}

func (i *mutStructIterator) Done() {}

func (i *mutStructIterator) Next(p *starlark.Value) bool {
	if i.ix >= len(i.keys) {
		return false
	}
	*p = i.s.vals[i.keys[i.ix]]
	i.ix++
	return true
}

func (s *mutStruct) Get(v starlark.Value) (starlark.Value, bool, error) {
	str, ok := v.(starlark.String)
	if !ok {
		return nil, false, fmt.Errorf("struct indexing: must use a string key, got %s", s.Type())
	}
	v, found := s.vals[str.GoString()]
	return v, found, nil
}

func (s *mutStruct) Iterate() starlark.Iterator {
	return &mutStructIterator{s: s, keys: s.vals.Keys(), ix: 0}
}

func (s *mutStruct) Items() []starlark.Tuple {
	items := make([]starlark.Tuple, 0, len(s.vals))
	for k, v := range s.vals {
		t := starlark.Tuple{starlark.String(k), v}
		items = append(items, t)
	}
	return items
}

func mutStructBuiltin(_ *starlark.Thread, _ *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("struct: unexpected positional arguments")
	}
	return mutStructFromKwargs(kwargs), nil
}

func mutStructFromKwargs(kwargs []starlark.Tuple) *mutStruct {
	s := &mutStruct{
		vals: make(starlark.StringDict),
	}
	for _, kwarg := range kwargs {
		k := string(kwarg[0].(starlark.String))
		v := kwarg[1]
		s.vals[k] = v
	}
	return s
}
