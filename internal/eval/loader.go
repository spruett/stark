package eval

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/spruett/stark/internal/config"
	"gitlab.com/spruett/stark/internal/eval/builtins"
	"go.starlark.net/starlark"
)

type loadSrc struct {
	filename string
	src      interface{}
}

func (s *loadSrc) contents() (string, error) {
	if c, ok := s.src.(string); ok {
		return c, nil
	}
	fd, err := os.Open(s.filename)
	if err != nil {
		return "", err
	}
	buf := new(strings.Builder)
	_, err = io.Copy(buf, fd)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func (s *loadSrc) saveToFile() (string, error) {
	if s.src == nil {
		panic("saveToFile() called with nil src")
	}

	file, err := ioutil.TempFile("/tmp", path.Base(s.filename))
	if err != nil {
		return "", err
	}

	fd, err := os.OpenFile(file.Name(), os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return "", err
	}
	defer fd.Close()
	_, err = fd.WriteString(s.src.(string))
	if err != nil {
		return "", err
	}
	return file.Name(), nil
}

func localFile(filename string) *loadSrc {
	return &loadSrc{filename: filename}
}

func withContent(name string, content string) *loadSrc {
	return &loadSrc{filename: name, src: content}
}

func httpPath(url string) (*loadSrc, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to download module %s: %v", url, err)
	}
	if response.StatusCode != 200 {
		return nil, fmt.Errorf("failed to download module %s: http %d: %s", url, response.StatusCode, response.Status)
	}
	buffer := new(strings.Builder)
	_, err = io.Copy(buffer, response.Body)
	if err != nil {
		return nil, err
	}
	return withContent(url, buffer.String()), nil
}

func packageRootPath(parentFile string, name string) (*loadSrc, error) {
	root, err := config.FindPackageRoot(parentFile)
	if err != nil {
		return nil, err
	}

	return localFile(path.Join(root, name)), nil
}

func pkgSource(parentFile string, target string) (*loadSrc, error) {
	cfg, err := config.FindAndLoadConfig(parentFile)
	if err != nil {
		return nil, err
	}
	if cfg == nil {
		return nil, fmt.Errorf("cannot use dependency %s: no starkcfg.star", target)
	}
	target = strings.TrimPrefix(target, "@")
	split := strings.SplitN(target, "/", 2)
	depName := split[0]
	path := split[1]

	if _, ok := cfg.Deps[depName]; !ok {
		return nil, fmt.Errorf("dependency %s not found in starkcfg.star", depName)
	}

	depPath := cfg.DependencyPath(depName)
	stat, _ := os.Stat(depPath)
	if stat == nil || !stat.IsDir() {
		return nil, fmt.Errorf("dependency %s not found locally: run stark fetch-deps to install", depName)
	}

	return localFile(filepath.Join(cfg.DependencyPath(depName), path)), nil
}

func loadSrcFromPath(parentFile string, name string) (*loadSrc, error) {
	if strings.HasPrefix(name, "//") {
		return packageRootPath(parentFile, strings.TrimPrefix(name, "//"))
	} else if strings.HasPrefix(name, "repo://") {
		return packageRootPath(parentFile, strings.TrimPrefix(name, "repo://"))
	} else if strings.HasPrefix(name, "http://") || strings.HasPrefix(name, "https://") {
		return httpPath(name)
	} else if strings.HasPrefix(name, "std/") {
		return stdlibSource(name)
	} else if strings.HasPrefix(name, "@") {
		return pkgSource(parentFile, name)
	}
	return nil, fmt.Errorf("unsupported scheme for loading file: %s", name)
}

func loadStarlark(thread *starlark.Thread, src *loadSrc) (starlark.StringDict, error) {
	b := allBuiltins(thread.Local(CtxKey))

	childThread := starlark.Thread{
		Load: thread.Load,
	}
	childThread.SetLocal(CtxKey, thread.Local(CtxKey))
	if src.filename != "" {
		childThread.SetLocal(FileKey, src.filename)
	}
	return starlark.ExecFile(&childThread, src.filename, src.src, b)
}

func loadRaw(src *loadSrc) (starlark.StringDict, error) {
	str, err := src.contents()
	if err != nil {
		return nil, err
	}
	return starlark.StringDict{"raw": starlark.String(str)}, nil
}

func hasAnySuffix(filename string, suffixes ...string) bool {
	for _, suffix := range suffixes {
		if strings.HasSuffix(filename, suffix) {
			return true
		}
	}
	return false
}

func loadFile(env *Env, thread *starlark.Thread, name string) (starlark.StringDict, error) {
	log.Trace().Str("path", name).Msg("loading starlark dependency")
	parentFile, _ := thread.Local(FileKey).(string)
	src, err := loadSrcFromPath(parentFile, name)
	if err != nil {
		return nil, err
	}

	if hasAnySuffix(name, ".star", ".stark") {
		return loadStarlark(thread, src)
	} else if hasAnySuffix(name, ".yaml", ".yml", ".json", ".txt", ".toml") {
		return loadRaw(src)
	} else if hasAnySuffix(name, ".proto", ".protobuf") {
		if src.src != nil {
			tmp, err := src.saveToFile()
			if err != nil {
				return nil, err
			}
			defer os.Remove(tmp)
			return loadProtoFile(tmp)
		}
		return loadProtoFile(src.filename)
	}
	return nil, fmt.Errorf("unsupported extension: %s", name)
}

func allBuiltins(ctx any) starlark.StringDict {
	builtins := builtins.BaseBuiltins()
	builtins["template"] = templateModule()
	if ctx != nil {
		builtins[CtxKey] = ctx.(starlark.Value)
	}
	return builtins
}
