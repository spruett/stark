package eval

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"gitlab.com/spruett/stark/internal/config"
	"gitlab.com/spruett/stark/internal/eval/builtins"
	"go.starlark.net/starlark"
)

type Env struct {
	thread *starlark.Thread

	exported *[]starlark.Value
	builtins starlark.StringDict

	cfg *config.StarkCfg
}

var CtxKey string = "ctx"
var FileKey string = "file"

func (e *Env) getExported() ([]starlark.Value, error) {
	if len(*e.exported) == 0 {
		return nil, fmt.Errorf("no value exported: use export() at the end of the config")
	}
	return *e.exported, nil
}

func NewEnv() *Env {
	exported := make([]starlark.Value, 0, 1)
	env := &Env{
		exported: &exported,
		builtins: allBuiltins(nil),
	}
	env.thread = &starlark.Thread{
		Load: func(thread *starlark.Thread, path string) (starlark.StringDict, error) {
			return loadFile(env, thread, path)
		},
	}

	env.thread.SetLocal(builtins.ExportKey, env.exported)

	return env
}

func (e *Env) AddCtx(val starlark.Value) {
	e.builtins["ctx"] = val
	e.thread.SetLocal(CtxKey, val)
}

func (e *Env) EvalFile(filename string) ([]starlark.Value, error) {
	log.Debug().Str("file", filename).Msg("evaluating file")
	e.thread.SetLocal(FileKey, filename)

	cfg, err := config.FindAndLoadConfig(filename)
	if err != nil {
		return nil, err
	}
	e.cfg = cfg

	_, err = starlark.ExecFile(e.thread, filename, nil, e.builtins)
	if err != nil {
		return nil, err
	}
	exported, err := e.getExported()
	return exported, err
}

func (e *Env) EvalString(value string, env starlark.StringDict) (starlark.Value, error) {
	vars := make(starlark.StringDict)
	for k, v := range e.builtins {
		vars[k] = v
	}
	for k, v := range env {
		vars[k] = v
	}

	return starlark.Eval(e.thread, "", value, vars)
}
