package eval

import (
	"fmt"
	"strings"
	"text/template"

	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
)

func templateModule() *starlarkstruct.Module {
	return &starlarkstruct.Module{
		Name: "template",
		Members: map[string]starlark.Value{
			"render": starlark.NewBuiltin("render", renderTemplateBuiltin),
		},
	}
}

func convertToTemplateFriendlyVal(val interface{}) interface{} {
	if v, ok := val.(starlark.IterableMapping); ok {
		m := make(map[interface{}]interface{})
		for _, item := range v.Items() {
			m[convertToTemplateFriendlyVal(item.Index(0))] = convertToTemplateFriendlyVal(item.Index(1))
		}
		return m
	} else if v, ok := val.(starlark.String); ok {
		return v.GoString()
	} else if v, ok := val.(starlark.Indexable); ok {
		l := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			l[i] = convertToTemplateFriendlyVal(v.Index(i))
		}
		return l
	}
	return val
}

func renderTemplate(
	parentFilename string,
	templatePath string,
	vars map[string]interface{},
) (string, error) {
	loadSrc, err := loadSrcFromPath(parentFilename, templatePath)
	if err != nil {
		return "", err
	}

	tpl, err := template.ParseFiles(loadSrc.filename)
	if err != nil {
		return "", err
	}

	buf := strings.Builder{}
	err = tpl.Execute(&buf, vars)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func renderTemplateBuiltin(
	thread *starlark.Thread,
	fn *starlark.Builtin,
	args starlark.Tuple,
	kwargs []starlark.Tuple,
) (starlark.Value, error) {
	parentFilename := thread.Local(FileKey).(string)
	if args.Len() != 1 {
		return nil, fmt.Errorf("%s: expected one argument filename, got %d", fn.Name(), args.Len())
	}
	filename := args.Index(0)
	filenameStr, ok := filename.(starlark.String)
	if !ok {
		return nil, fmt.Errorf("%s: expected filename to be a string, got %s", fn.Name(), filename.Type())
	}

	vars := make(map[string]interface{})
	for _, kwarg := range kwargs {
		if kwarg.Len() != 2 {
			return nil, fmt.Errorf("%s: kwarg expected to be a 2-tuple", fn.Name())
		}
		k := kwarg.Index(0)
		v := kwarg.Index(1)

		keyName, ok := k.(starlark.String)
		if !ok {
			return nil, fmt.Errorf("%s: kwarg key expected to be string, got %s", fn.Name(), k.Type())
		}
		vars[keyName.GoString()] = convertToTemplateFriendlyVal(v)
	}

	content, err := renderTemplate(parentFilename, filenameStr.GoString(), vars)
	if err != nil {
		return nil, err
	}
	return starlark.String(content), nil
}
