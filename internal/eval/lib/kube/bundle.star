def _apply_target_namespace(
    resource,
    target_namespace,
):
    resource_namespace = get(resource.metadata, 'namespace')
    if resource_namespace and target_namespace != resource_namespace:
        fail(
            "{}/{}: different namespace from target: {} vs {}".format(
                resource.kind,
                resource.metadata.name,
                resource_namespace,
                target_namespace,
            )
        )
    
    resource.metadata.namespace = target_namespace

def _apply_labels(
    resource,
    labels,
):
    resource_labels = get(resource.metadata, 'labels', {})
    for k, v in labels.items():
        resource_labels[k] = v
    
    resource.metadata.labels = resource_labels


def _default_labels(
    bundle_name,
):
    labels = {
        'app.kubernetes.io/managed-by': 'stark',
        'app.kubernetes.io/part-of': bundle_name,
    }
    if 'version' in ctx:
        labels['app.kubernetes.io/version'] = ctx['version']
    return labels

def bundle(
    name,
    resources=[],
    target=None,
    labels={},
):
    labels = dict(
        _default_labels(name),
        **labels,
    )

    if target:
        namespace = get(target, 'namespace')
        if namespace:
            for resource in resources:
                _apply_target_namespace(resource, namespace)
    
    for resource in resources:
        _apply_labels(resource, labels)
    return struct(
        kind='StarkBundle',
        metadata=struct(
            name=name,
            labels=labels,
        ),
        target=target,
        resources=resources,
    )