package eval

import (
	_ "embed"
	"fmt"
)

//go:embed lib/kube/bundle.star
var stdlibKubeBundle string

//go:embed lib/generate.star
var stdlibGenerate string

func stdlibSource(name string) (*loadSrc, error) {
	switch name {
	case "std/kube/bundle.star":
		return withContent(name, stdlibKubeBundle), nil
	case "std/generate.star":
		return withContent(name, stdlibGenerate), nil
	default:
		return nil, fmt.Errorf("unknown stdlib module: %s", name)
	}
}
