package encoding

import (
	"encoding/json"
	"fmt"
	"strings"

	"go.starlark.net/starlark"
	"gopkg.in/yaml.v3"

	"gitlab.com/spruett/stark/internal/eval"
)

type YamlEncoder struct {
	Env *eval.Env
}

func (y *YamlEncoder) Encode(value starlark.Value) (string, error) {
	js, err := y.Env.EvalString("json.encode(v)", starlark.StringDict{"v": value})
	if err != nil {
		return "", nil
	}
	str, ok := js.(starlark.String)
	if !ok {
		return "", fmt.Errorf("expected string: %v", js)
	}

	var msg interface{}
	err = json.Unmarshal([]byte(str.GoString()), &msg)
	if err != nil {
		return "", err
	}

	buf := new(strings.Builder)
	encoder := yaml.NewEncoder(buf)
	err = encoder.Encode(msg)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func (y *YamlEncoder) EncodeMany(values []starlark.Value) (string, error) {
	buffer := new(strings.Builder)
	for i, v := range values {
		yaml, err := y.Encode(v)
		if err != nil {
			return "", err
		}
		buffer.WriteString(yaml)
		if i != len(values)-1 {
			buffer.WriteString("---\n")
		}
	}
	return buffer.String(), nil
}
