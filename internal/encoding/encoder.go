package encoding

import "go.starlark.net/starlark"

type Encoder interface {
	Encode(value starlark.Value) (string, error)
	EncodeMany(values []starlark.Value) (string, error)
}
