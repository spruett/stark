package encoding

import (
	"fmt"

	"go.starlark.net/starlark"

	"gitlab.com/spruett/stark/internal/eval"
)

type JsonEncoder struct {
	Env *eval.Env
}

func (j *JsonEncoder) Encode(value starlark.Value) (string, error) {
	val, err := j.Env.EvalString("json.encode(v)", starlark.StringDict{"v": value})
	if err != nil {
		return "", nil
	}
	str, ok := val.(starlark.String)
	if !ok {
		return "", fmt.Errorf("expected string: %v", val)
	}
	return str.GoString(), nil
}

func (j *JsonEncoder) EncodeMany(values []starlark.Value) (string, error) {
	if len(values) == 1 {
		return j.Encode(values[0])
	}
	return j.Encode(starlark.NewList(values))
}
