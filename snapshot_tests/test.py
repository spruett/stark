#!/usr/bin/env python3

import argparse
import json
import re
import subprocess
import difflib
import sys
import tempfile
import os

parser = argparse.ArgumentParser(
    "test.py",
)

parser.add_argument("--stark", help="Path to stark binary", default="./stark")
parser.add_argument("--config", help="Path to test config", default="./snapshot_tests/config.star")
parser.add_argument("--save", help="If true, overwrite output files instead of checking", default=False, action="store_true")
parser.add_argument("--filter", help="Filter tests by output file (regex)", default=".*")


def sanitize_error_filenames(output):
    regex = r'(/\w+)+/stark/snapshot_tests/'
    return re.sub(regex, '', output)


def get_stark_output(cli, cmd, error=False):
    stark_bin = cli.stark
    if error:
        subprocess.check_output([stark_bin] + cmd, stderr=subprocess.STDOUT)
    return subprocess.check_output([stark_bin] + cmd)


def run_stark_build(cli, test, opts=[]):
    error = test.get('expected_error')
    cmd = ['build'] + test.get('options', []) + [test['input']] + opts
    if error:
        try:
            get_stark_output(cli, cmd, error=True)
            raise Exception('build should have failed, but succeeded')
        except subprocess.CalledProcessError as exc:
            return sanitize_error_filenames(exc.output.decode('utf-8'))
    else:
        return get_stark_output(
            cli,
            cmd,
            error=error
        ).decode('utf-8')


def run_stark_gen(cli, test, opts=[], outdir=None):
    with tempfile.TemporaryDirectory(prefix='stark') as tmpdir:
        if outdir is None:
            outdir = tmpdir
        get_stark_output(
            cli,
            ['gen'] + test.get('options', []) + [test['input']] + opts + ["-o", outdir],
        )
        expected_filename = os.path.join(outdir, os.path.basename(test['output']))
        with open(expected_filename) as fd:
            return fd.read()


def run_stark_fmt(cli, test):
    cmd = ['fmt', test['input']]
    return get_stark_output(cli, cmd).decode('utf-8')


def get_test_config(cli):
    built_config = get_stark_output(cli, ["build", "-fjson", cli.config])
    parsed_config = json.loads(built_config)
    return parsed_config


def print_diff(built, expected):
    built = [line + "\n" for line in built.split('\n')]
    expected = [line + "\n" for line in expected.split('\n')]
    diff = difflib.context_diff(expected, built, fromfile="expected", tofile="built")
    sys.stdout.writelines(diff)


def check_diff(output, test):
    with open(test['output']) as fd:
        expected_output = fd.read()
    
    if output.strip() != expected_output.strip():
        print('{}: FAIL'.format(test['output']))
        print_diff(output, expected_output)
        return False

    
    print('{}: ok'.format(test['output']))
    return True


def run_build_test(cli, test_opts):
    built_output = run_stark_build(cli, test_opts)
    return check_diff(built_output, test_opts)


def overwrite_build_test(cli, test_opts):
    built_output = run_stark_build(cli, test_opts)
    out_file = test_opts['output']
    with open(out_file, 'w') as fd:
        fd.write(built_output)
    print('{}: overwritten'.format(test_opts['output']))


def run_gen_test(cli, test_opts):
    gen_output = run_stark_gen(cli, test_opts)
    return check_diff(gen_output, test_opts)


def overwrite_gen_test(cli, test_opts):
    run_stark_gen(cli, test_opts, outdir=os.path.dirname(test_opts['output']))
    print('{}: overwritten'.format(test_opts['output']))


def run_fmt_test(cli, test_opts):
    gen_output = run_stark_fmt(cli, test_opts)
    return check_diff(gen_output, test_opts)


def overwrite_fmt_test(cli, test_opts):
    output = run_stark_fmt(cli, test_opts)
    with open(test_opts['output'], 'w') as fd:
        fd.write(output)
    print('{}: overwritten'.format(test_opts['output']))

test_types = {
    'build': {
        'run': run_build_test,
        'overwrite': overwrite_build_test,
    },
    'gen': {
        'run': run_gen_test,
        'overwrite': overwrite_gen_test,
    },
    'fmt': {
        'run': run_fmt_test,
        'overwrite': overwrite_fmt_test,
    }
}

def main():
    cli = parser.parse_args()
    cfg = get_test_config(cli)
    all_passed = True
    for test in cfg:
        if not re.search(cli.filter, test['output']):
            continue

        test_handlers = test_types[test['type']]
        overwrite = test_handlers['overwrite']
        run = test_handlers['run']
        if cli.save:
            overwrite(cli, test)
            continue

        if not run(cli, test):
            all_passed = False

    if not all_passed:
        sys.exit(1)


if __name__ == '__main__':
    main()