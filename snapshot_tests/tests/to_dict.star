s = struct(
    a=1,
    b=2,
    c=3,
    d=5
)

d = to_dict(s)
d['d'] = 4

export(struct(**d))