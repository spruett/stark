export(
    yaml.decode(
        template.render(
            '//lib/template.yaml.tpl',
            num_val=123,
            str_val='abc',
            dict_val={'a': 1, 'b': 'xyz'},
            struct_val=struct(a=1, b='abc'),
        )
    )
)