s = struct(
    a=1,
    b=2,
    c=3,
    d=5,
    xs=[1,2]
)

s.d = 4
s.xs.append(3)

export(s)