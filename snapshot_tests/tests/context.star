x = {
    'version': ctx.get('version', 'latest'),
    'static': 'static',
    'other': ctx.get('other', 'abc')
}

export(x)