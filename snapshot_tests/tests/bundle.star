load('std/kube/bundle.star', 'bundle')
load(
    '//lib/k8s.star',
    'simple_deployment',
    'container',
    'container_port',
    'service_for',
)

version = ctx.get('version', 'latest')

deploy = simple_deployment(
    'nginx',
    containers=[
        container('nginx:{}'.format(version), ports=[container_port(80)])
    ]
)
svc = service_for(deploy)

export(
    bundle(
        'nginx',
        target=struct(
            namespace='apps',
        ),
        resources=[
            deploy,
            svc,
        ]
    )
)