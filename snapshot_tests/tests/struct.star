s = struct(a = '123')
v = get(s, 'c', 'default')

export(s + struct(b = 'xyz') + struct(d = v))