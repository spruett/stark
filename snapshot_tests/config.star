tests = []

def add_test(name, output=None, options=[], test_type='build', error=False):
    if not output:
        output = name
    ext = 'yaml'
    if error:
        ext = 'txt'
    elif test_type == 'fmt':
        ext = 'star'

    test_opts = {
        "input": "./snapshot_tests/tests/{}.star".format(name),
        "output": "./snapshot_tests/outputs/{}.{}".format(output, ext),
        "options": options,
        "type": test_type,
        "expected_error": error,
    }
    tests.append(test_opts)

add_test('simple')
add_test('simple', 'simple-json', ['-f', 'json'])
add_test('import')
add_test('context', 'context-default')
add_test('context', 'context-set', ['--version', 'V2', '--context', 'other=xyz'])
add_test('proto')
add_test('struct')
add_test('to_dict')
add_test('struct_mutable')
add_test('bundle')
add_test('template')

add_test('generated', test_type='gen')
add_test('fmt', test_type='fmt')

add_test('syntax_error', error=True)
add_test('missing_dep', error=True)
add_test('missing_std', error=True)

export(tests)