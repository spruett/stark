# Features
 - DONE: Template engine
 - DONE: Deployment monitoring
    - DONE: Status watching via k8s API for deployment, statefulset, daemonset
 - DONE: Code generator
    - DONE: `generate.star` library, to bundle {"object": {...}, "format": "yaml", "target": "path/to"}
    - DONE: `generate` CLI command
 - DONE: Dependency management
    - DONE: Design `starkcfg.star` config file
    - DONE: Design dependency downloads, import syntax
      - DONE: maybe OCI artifact format: https://oras.land/client_libraries/0_go/
    - DONE: Dependency downloader
 
# Speculative features
 - TODO: Terraform (?)
 - TODO: HCL format
 - TODO: ArgoCD config plugin
 - TODO: script runner
   - TODO: `stark run test`: configure scripts like npm

# Refactoring
 - TODO: use `errors` with cause
 - TODO: Merge FileArgs, ContextArgs, add utility (?)
 - DONE: Should `bundle` be renamed?
    - DONE: Move to kube subcommand, `std/kube.star:bundle`?

# Docs
 - TODO: Add a README.md
 - TODO: Intro docs, starlark overview
 - TODO: Docs for bundles, k8s deployments
 - TODO: Deploy docs as a website

# Testing
 - TODO: integration tests
   - TODO: framework, CI
   - TODO: deployment tests
   - TODO: package upload/download tests
 - TODO: Formatting snapshot tests
 - DONE: Add error handling to snapshot tests
 
# Codebase
 - TODO: General comments/documentation
 - TODO: Make code `revive` clean
    - TODO: Add `revive` config
